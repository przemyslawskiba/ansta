import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by pskiba on 18.08.2017.
 */
public class Exc3 {


    public static void main(String[] args) {

        double step = 0.5;
        double start = 2;
        double stop = 5.5;
        double sum = start;

        NumberFormat format = new DecimalFormat("#.#");

        do {
            System.out.println(format.format(sum));
            sum = sum + step;
        } while (sum <= stop);
    }
}
