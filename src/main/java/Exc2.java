/**
 * Created by pskiba on 18.08.2017.
 */
public class Exc2 {

    public static void main(String[] args) {

        int table[] = {2, 3, 8, 7, 4, 9};
        int n = 10;
        boolean checkNumber;

        for (int i = 1; i <= n; i++) {
            checkNumber = true;
            for (int a : table) {
                if (i == a) {
                    checkNumber = false;
                }
            }
            if (checkNumber) {
                System.out.println(i);
            }
        }
    }
}
