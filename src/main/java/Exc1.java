/**
 * Created by pskiba on 18.08.2017.
 */
public class Exc1 {

    public static void main(String[] args) {

        String startCode = "76-900";
        String endCode = "80-155";

        startCode = startCode.replace("-", "");
        endCode = endCode.replace("-", "");

        int newStartCode = Integer.parseInt(startCode);
        int newEndCode = Integer.parseInt(endCode);
        System.out.println("from " + newStartCode + " to " + newEndCode);

        if (newStartCode < newEndCode) {
            for (int a = newStartCode; a <= newEndCode; a++) {
                String result = Integer.toString(a);
                System.out.println(result.substring(0, 2) + "-" + result.substring(2));
            }
        } else {
            for (int a = newStartCode; a >= newEndCode; a--) {
                String result = Integer.toString(a);
                System.out.println(result.substring(0, 2) + "-" + result.substring(2));
            }
        }
    }
}
